output "subnets" {
  value = ["${data.aws_subnet.default.*.id}"]
}
output "zones" {
  value = ["${data.aws_subnet.default.*.availability_zone}"]
}
output "security_group_backend" {
  value = "${aws_security_group.backend.id}"
}
output "target_group_main" {
  value = "${aws_alb_target_group.main.id}"
}
output "target_group_security" {
  value = "${aws_alb_target_group.security.id}"
}
output "vpc" {
  value = "${data.aws_vpc.default.id}"
}
