resource "aws_alb" "main" {
  provider = "aws.network"
  count = "${var.lb_count}"
  name = "mirror-${var.env}-${var.region_suffix}-main"
  ip_address_type = "dualstack"
  security_groups = ["${aws_security_group.lb.id}"]
  subnets = ["${data.aws_subnet.default.*.id}"]

  tags {
    Name = "mirror-${var.env}-main"
    Project = "mirror"
  }
}

resource "aws_alb_listener" "main" {
  provider = "aws.network"
  count = "${var.lb_count}"
  load_balancer_arn = "${aws_alb.main.arn}"
  port = 80
  protocol = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.main.arn}"
    type = "forward"
  }
}

resource "aws_alb_target_group" "main" {
  provider = "aws.network"
  count = "${var.lb_count}"
  name = "mirror-${var.env}-${var.region_suffix}-main"
  port = 80
  protocol = "HTTP"
  vpc_id = "${data.aws_vpc.default.id}"

  health_check {
    path = "/.online"
  }

  tags {
    Name = "mirror-${var.env}-main"
    Project = "mirror"
  }
}

resource "aws_route53_record" "main" {
  provider = "aws.global"
  count = "${var.lb_count}"
  zone_id = "${var.route53_zone}"
  name = "b-m"
  type = "A"
  set_identifier = "backend-${var.region_suffix}"

  alias {
    name = "${aws_alb.main.dns_name}"
    zone_id = "${aws_alb.main.zone_id}"
    evaluate_target_health = false
  }

  latency_routing_policy {
    region = "${var.region}"
  }
}
