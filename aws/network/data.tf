data "aws_availability_zone" "default" {
  provider = "aws.network"
  count = "${length(var.zones)}"
  name = "${var.zones[count.index]}"
}

data "aws_subnet" "default" {
  provider = "aws.network"
  count = "${length(var.zones)}"

  filter {
    name = "vpc-id"
    values = ["${data.aws_vpc.default.id}"]
  }
  filter {
    name = "availabilityZone"
    values = ["${data.aws_availability_zone.default.*.name[count.index]}"]
  }
  filter {
    name = "tag:Name"
    values = ["mirror"]
  }
}

data "aws_vpc" "default" {
  provider = "aws.network"

  filter {
    name = "tag:Project"
    values = ["mirror"]
  }
}
