module "syncproxy" {
  source = "./syncproxy"
  provider_access_key = "${var.provider_access_key}"
  provider_secret_key = "${var.provider_secret_key}"
  env = "${var.env}"
  region = "eu-central-1"
  zone = "${module.network-eu-central-1.zones[0]}"

  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  instance_ami = "${var.instance_ami}"
  instance_type = "${var.instance_type_syncproxy}"
  route53_zone = "${aws_route53_zone.default.zone_id}"
  subnet = "${module.network-eu-central-1.subnets[0]}"
  vpc = "${module.network-eu-central-1.vpc}"
}
