module "monitor" {
  source = "./monitor"
  provider_access_key = "${var.provider_access_key}"
  provider_secret_key = "${var.provider_secret_key}"
  env = "${var.env}"
  region = "eu-central-1"
  zone = "${module.network-eu-central-1.zones[1]}"

  instance_ami = "${var.instance_ami}"
  instance_type = "${var.instance_type_monitor}"
  route53_zone = "${aws_route53_zone.default.zone_id}"
  subnet = "${module.network-eu-central-1.subnets[1]}"
  vpc = "${module.network-eu-central-1.vpc}"
}
