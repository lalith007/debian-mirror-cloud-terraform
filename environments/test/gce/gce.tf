module "gce" {
  source = "../../../gce"

  provider_credentials_file = "${var.gce_credentials_file}"
  provider_project = "debian-infra"
  env = "test"

  disk_size_main = 1600
  disk_size_security = 150
  disk_snapshot_main = "mirror-main-20170820"
  disk_snapshot_security = "mirror-security-20170820"
  instance_count_backend = {
    europe-west1 = 2
  }
  instance_image = "debian-cloud/debian-9"
  instance_ip_jump = "35.189.252.57"
  instance_ip_monitor = "35.195.178.1"
  instance_type_backend = "n1-standard-2"
  instance_type_monitor = "g1-small"
  instance_type_syncproxy = "n1-standard-2"
  lb_global_ip = [
    "35.190.93.121",
    "2600:1901:0:6729::",
  ]
  zone_jump = "europe-west1-d"
  zone_monitor = "europe-west1-d"
  zone_syncproxy = "europe-west1-c"
}

variable "gce_credentials_file" {}
