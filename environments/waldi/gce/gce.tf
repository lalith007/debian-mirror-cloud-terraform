module "gce" {
  source = "../../../gce"

  provider_credentials_file = "${var.gce_credentials_file}"
  provider_project = "debian-cloud-experiments"
  env = "waldi"

  disk_size_main = 20
  disk_size_security = 10
  disk_snapshot_main = ""
  disk_snapshot_security = ""
  instance_count_backend = {
    asia-east1 = 4
    australia-southeast1 = 4
    europe-west1 = 4
    us-central1 = 4
  }
  instance_image = "debian-cloud/debian-9"
  instance_ip_jump = "35.195.203.253"
  instance_ip_monitor = "35.195.163.127"
  instance_type_backend = "g1-small"
  instance_type_monitor = "g1-small"
  instance_type_syncproxy = "g1-small"
  lb_global_ip = [
    "35.190.60.39",
    "2600:1901:0:4cb2::",
  ]
  zone_etcd = [
    "europe-west1-b",
    "europe-west1-c",
    "europe-west1-d",
  ]
  zone_jump = "europe-west1-d"
  zone_monitor = "europe-west1-d"
  zone_syncproxy = "europe-west1-c"
}

variable "gce_credentials_file" {}
