module "backend-asia-east1-a" {
  source = "./backend"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  env = "${var.env}"
  instance_count = "${ceil(lookup(var.instance_count_backend, "asia-east1", 0) / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_type = "${var.instance_type_backend}"
  zone = "asia-east1-a"
  zone_number = 0
  zone_suffix ="ase1a"
}

module "backend-asia-east1-c" {
  source = "./backend"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  env = "${var.env}"
  instance_count = "${floor(lookup(var.instance_count_backend, "asia-east1", 0) / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_type = "${var.instance_type_backend}"
  zone = "asia-east1-c"
  zone_number = 1
  zone_suffix ="ase1c"
}

module "backend-asia-northeast1-a" {
  source = "./backend"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  env = "${var.env}"
  instance_count = "${ceil(lookup(var.instance_count_backend, "asia-northeast1", 0) / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_type = "${var.instance_type_backend}"
  zone = "asia-northeast1-a"
  zone_number = 0
  zone_suffix ="asne1a"
}

module "backend-asia-northeast1-b" {
  source = "./backend"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  env = "${var.env}"
  instance_count = "${floor(lookup(var.instance_count_backend, "asia-northeast1", 0) / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_type = "${var.instance_type_backend}"
  zone = "asia-northeast1-b"
  zone_number = 1
  zone_suffix ="asne1b"
}

module "backend-australia-southeast1-a" {
  source = "./backend"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  env = "${var.env}"
  instance_count = "${ceil(lookup(var.instance_count_backend, "australia-southeast1", 0) / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_type = "${var.instance_type_backend}"
  zone = "australia-southeast1-a"
  zone_number = 0
  zone_suffix ="ause1a"
}

module "backend-australia-southeast1-b" {
  source = "./backend"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  env = "${var.env}"
  instance_count = "${floor(lookup(var.instance_count_backend, "australia-southeast1", 0) / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_type = "${var.instance_type_backend}"
  zone = "australia-southeast1-c"
  zone_number = 1
  zone_suffix ="ause1b"
}

module "backend-europe-west1-c" {
  source = "./backend"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  env = "${var.env}"
  instance_count = "${ceil(lookup(var.instance_count_backend, "europe-west1", 0) / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_type = "${var.instance_type_backend}"
  zone = "europe-west1-c"
  zone_number = 0
  zone_suffix ="euw1c"
}

module "backend-europe-west1-d" {
  source = "./backend"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  env = "${var.env}"
  instance_count = "${floor(lookup(var.instance_count_backend, "europe-west1", 0) / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_type = "${var.instance_type_backend}"
  zone = "europe-west1-d"
  zone_number = 1
  zone_suffix ="euw1d"
}

module "backend-us-central1-c" {
  source = "./backend"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  env = "${var.env}"
  instance_count = "${ceil(lookup(var.instance_count_backend, "us-central1", 0) / 2.0 )}"
  instance_image = "${var.instance_image}"
  instance_type = "${var.instance_type_backend}"
  zone = "us-central1-c"
  zone_number = 0
  zone_suffix ="usc1c"
}

module "backend-us-central1-f" {
  source = "./backend"
  disk_size_main = "${var.disk_size_main}"
  disk_size_security = "${var.disk_size_security}"
  disk_snapshot_main = "${var.disk_snapshot_main}"
  disk_snapshot_security = "${var.disk_snapshot_security}"
  env = "${var.env}"
  instance_count = "${floor(lookup(var.instance_count_backend, "us-central1", 0) / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_type = "${var.instance_type_backend}"
  zone = "us-central1-f"
  zone_number = 1
  zone_suffix ="usc1f"
}
