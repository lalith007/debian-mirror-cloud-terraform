resource "google_compute_firewall" "internal" {
  name = "mirror-${var.env}-allow-internal"
  network = "mirror"

  allow {
    protocol = "all"
  }

  source_tags = [
    "mirror-${var.env}",
  ]
  target_tags = [
    "mirror-${var.env}",
  ]
}

resource "google_compute_firewall" "external-backend" {
  name = "mirror-${var.env}-backend-allow-external"
  network = "mirror"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = [
    "0.0.0.0/0",
  ]
  target_tags = [
    "mirror-${var.env}-backend",
  ]
}

resource "google_compute_firewall" "external-monitor" {
  name = "mirror-${var.env}-monitor-allow-external"
  network = "mirror"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = [
    "0.0.0.0/0",
  ]
  target_tags = [
    "mirror-${var.env}-monitor",
  ]
}
