variable "provider_credentials_file" { }
variable "provider_project" { }
variable "env" { }
variable "disk_size_main" { }
variable "disk_size_security" { }
variable "disk_snapshot_main" { }
variable "disk_snapshot_security" { }
variable "instance_count_backend" {
  type = "map"
}
variable "instance_image" { }
variable "instance_ip_jump" {
  default = ""
}
variable "instance_ip_monitor" {
  default = ""
}
variable "instance_type_backend" { }
variable "instance_type_monitor" { }
variable "instance_type_syncproxy" { }
variable "lb_global_ip" {
  type = "list"
  default = [""]
}
variable "zone_monitor" { }
variable "zone_jump" { }
variable "zone_syncproxy" { }
