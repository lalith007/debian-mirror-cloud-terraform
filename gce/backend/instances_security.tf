resource "google_compute_instance" "backend-security" {
  count = "${var.instance_count}"
  name = "mirror-${var.env}-b-s-${var.zone_suffix}${count.index}"
  zone = "${var.zone}"
  machine_type = "${var.instance_type}"

  labels = {
    environment = "${var.env}",
    instance = "${var.zone_number + count.index * 2}",
    purpose = "backend-security",
    project = "mirror",
  }

  tags = [
    "instance---${var.zone_number + count.index * 2}",
    "purpose---backend-security",
    "mirror-${var.env}",
    "mirror-${var.env}-backend",
  ]

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
      type = "pd-ssd"
    }
  }

  attached_disk {
    source = "${google_compute_disk.backend-security.*.self_link[count.index]}"
  }

  network_interface {
    subnetwork = "mirror"
  }

  lifecycle {
    ignore_changes = [ "machine_type" ]
  }
}

resource "google_compute_disk" "backend-security" {
  count = "${var.instance_count}"
  name = "mirror-${var.env}-b-s-${var.zone_suffix}${count.index}-data"
  zone = "${var.zone}"
  size = "${var.disk_size_security}"
  snapshot = "${var.disk_snapshot_security}"
  type = "pd-ssd"

  lifecycle {
    ignore_changes = [ "snapshot" ]
  }
}

resource "google_compute_instance_group" "backend-security" {
  name = "mirror-${var.env}-b-s-${var.zone_suffix}"
  zone = "${var.zone}"

  instances = [
    "${google_compute_instance.backend-security.*.self_link}"
  ]
}
