variable "disk_size_main" { }
variable "disk_size_security" { }
variable "disk_snapshot_main" {
}
variable "disk_snapshot_security" {
}
variable "env" {
}
variable "instance_count" {
}
variable "instance_image" {
}
variable "instance_type" {
}
variable "zone" {
}
variable "zone_number" { }
variable "zone_suffix" {
}
