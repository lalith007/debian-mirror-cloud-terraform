output "instances.main" {
  value = ["${google_compute_instance.backend-main.*.name}"]
}

output "instances.security" {
  value = ["${google_compute_instance.backend-security.*.name}"]
}

output "group.main" {
  value = "${google_compute_instance_group.backend-main.self_link}"
}

output "group.security" {
  value = "${google_compute_instance_group.backend-security.self_link}"
}
