provider "google" {
  credentials = "${file(var.provider_credentials_file)}"
  project     = "${var.provider_project}"
  region      = "us-central1"
}
