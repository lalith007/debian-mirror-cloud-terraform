resource "google_compute_instance" "monitor" {
  name = "mirror-${var.env}-m"
  zone = "${var.zone_monitor}"
  machine_type = "${var.instance_type_monitor}"

  labels = {
    environment = "${var.env}",
    purpose = "monitor",
    project = "mirror",
  }

  tags = [
    "purpose---monitor",
    "mirror-${var.env}",
    "mirror-${var.env}-monitor",
  ]

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
      type = "pd-ssd"
    }
  }

  network_interface {
    subnetwork = "mirror"
    access_config {
      nat_ip = "${var.instance_ip_monitor}"
    }
  }

  lifecycle {
    ignore_changes = [ "machine_type" ]
  }
}
