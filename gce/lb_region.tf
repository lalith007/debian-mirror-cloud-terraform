module "lb-asia-east1" {
  source = "./lb_region"
  env = "${var.env}"
  health_checks = ["${google_compute_http_health_check.default.name}"]
  instances_main = [
    "${formatlist("asia-east1-a/%s", module.backend-asia-east1-a.instances.main)}",
    "${formatlist("asia-east1-c/%s", module.backend-asia-east1-c.instances.main)}",
  ]
  instances_security = [
    "${formatlist("asia-east1-a/%s", module.backend-asia-east1-a.instances.security)}",
    "${formatlist("asia-east1-c/%s", module.backend-asia-east1-c.instances.security)}",
  ]
  region = "asia-east1"
  region_suffix ="ase1"
}

module "lb-asia-northeast1" {
  source = "./lb_region"
  env = "${var.env}"
  health_checks = ["${google_compute_http_health_check.default.name}"]
  instances_main = [
    "${formatlist("asia-northeast1-a/%s", module.backend-asia-northeast1-a.instances.main)}",
    "${formatlist("asia-northeast1-b/%s", module.backend-asia-northeast1-b.instances.main)}",
  ]
  instances_security = [
    "${formatlist("asia-northeast1-a/%s", module.backend-asia-northeast1-a.instances.security)}",
    "${formatlist("asia-northeast1-b/%s", module.backend-asia-northeast1-b.instances.security)}",
  ]
  region = "asia-northeast1"
  region_suffix ="asne1"
}

module "lb-australia-southeast1" {
  source = "./lb_region"
  env = "${var.env}"
  health_checks = ["${google_compute_http_health_check.default.name}"]
  instances_main = [
    "${formatlist("australia-southeast1-a/%s", module.backend-australia-southeast1-a.instances.main)}",
    "${formatlist("australia-southeast1-c/%s", module.backend-australia-southeast1-b.instances.main)}",
  ]
  instances_security = [
    "${formatlist("australia-southeast1-a/%s", module.backend-australia-southeast1-a.instances.security)}",
    "${formatlist("australia-southeast1-c/%s", module.backend-australia-southeast1-b.instances.security)}",
  ]
  region = "australia-southeast1"
  region_suffix ="ause1"
}

module "lb-europe-west1" {
  source = "./lb_region"
  env = "${var.env}"
  health_checks = ["${google_compute_http_health_check.default.name}"]
  instances_main = [
    "${formatlist("europe-west1-c/%s", module.backend-europe-west1-c.instances.main)}",
    "${formatlist("europe-west1-d/%s", module.backend-europe-west1-d.instances.main)}",
  ]
  instances_security = [
    "${formatlist("europe-west1-c/%s", module.backend-europe-west1-c.instances.security)}",
    "${formatlist("europe-west1-d/%s", module.backend-europe-west1-d.instances.security)}",
  ]
  region = "europe-west1"
  region_suffix ="euw1"
}

module "lb-us-central1" {
  source = "./lb_region"
  env = "${var.env}"
  health_checks = ["${google_compute_http_health_check.default.name}"]
  instances_main = [
    "${formatlist("us-central1-c/%s", module.backend-us-central1-c.instances.main)}",
    "${formatlist("us-central1-f/%s", module.backend-us-central1-f.instances.main)}",
  ]
  instances_security = [
    "${formatlist("us-central1-c/%s", module.backend-us-central1-c.instances.security)}",
    "${formatlist("us-central1-f/%s", module.backend-us-central1-f.instances.security)}",
  ]
  region = "us-central1"
  region_suffix ="usc1"
}
