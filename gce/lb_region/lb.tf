resource "google_compute_target_pool" "main" {
  count = "${length(var.instances_main) > 0 ? 1 : 0}"
  name = "mirror-${var.env}-b-m-${var.region_suffix}"
  region = "${var.region}"
  instances = ["${var.instances_main}"]
  health_checks = ["${var.health_checks}"]
}

resource "google_compute_forwarding_rule" "main" {
  count = "${length(var.instances_main) > 0 ? 1 : 0}"
  name = "mirror-${var.env}-b-m-${var.region_suffix}"
  region = "${var.region}"
  target = "${google_compute_target_pool.main.self_link}"
  port_range = "80"
}

resource "google_compute_target_pool" "security" {
  count = "${length(var.instances_security) > 0 ? 1 : 0}"
  name = "mirror-${var.env}-b-s-${var.region_suffix}"
  region = "${var.region}"
  instances = ["${var.instances_security}"]
  health_checks = ["${var.health_checks}"]
}

resource "google_compute_forwarding_rule" "security" {
  count = "${length(var.instances_security) > 0 ? 1 : 0}"
  name = "mirror-${var.env}-b-s-${var.region_suffix}"
  region = "${var.region}"
  target = "${google_compute_target_pool.security.self_link}"
  port_range = "80"
}
