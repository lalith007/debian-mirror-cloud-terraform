variable "env" { }
variable "health_checks" {
  type = "list"
}
variable "instances_main" {
  type = "list"
}
variable "instances_security" {
  type = "list"
}
variable "region" { }
variable "region_suffix" { }
