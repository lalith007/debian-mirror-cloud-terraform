resource "google_compute_backend_service" "main" {
  name = "mirror-${var.env}-b-m"
  port_name = "http"
  protocol = "HTTP"
  timeout_sec = 120

  backend {
    group = "${module.backend-asia-east1-a.group.main}"
  }
  backend {
    group = "${module.backend-asia-east1-c.group.main}"
  }
  backend {
    group = "${module.backend-asia-northeast1-a.group.main}"
  }
  backend {
    group = "${module.backend-asia-northeast1-b.group.main}"
  }
  backend {
    group = "${module.backend-australia-southeast1-a.group.main}"
  }
  backend {
    group = "${module.backend-australia-southeast1-b.group.main}"
  }
  backend {
    group = "${module.backend-europe-west1-c.group.main}"
  }
  backend {
    group = "${module.backend-europe-west1-d.group.main}"
  }
  backend {
    group = "${module.backend-us-central1-c.group.main}"
  }
  backend {
    group = "${module.backend-us-central1-f.group.main}"
  }

  health_checks = ["${google_compute_http_health_check.default.self_link}"]

  lifecycle {
    ignore_changes = [
      "enable_cdn",
    ]
  }
}

resource "google_compute_backend_service" "security" {
  name = "mirror-${var.env}-b-s"
  port_name = "http"
  protocol = "HTTP"
  timeout_sec = 120

  backend {
    group = "${module.backend-asia-east1-a.group.security}"
  }
  backend {
    group = "${module.backend-asia-east1-c.group.security}"
  }
  backend {
    group = "${module.backend-asia-northeast1-a.group.security}"
  }
  backend {
    group = "${module.backend-asia-northeast1-b.group.security}"
  }
  backend {
    group = "${module.backend-australia-southeast1-a.group.security}"
  }
  backend {
    group = "${module.backend-australia-southeast1-b.group.security}"
  }
  backend {
    group = "${module.backend-europe-west1-c.group.security}"
  }
  backend {
    group = "${module.backend-europe-west1-d.group.security}"
  }
  backend {
    group = "${module.backend-us-central1-c.group.security}"
  }
  backend {
    group = "${module.backend-us-central1-f.group.security}"
  }

  health_checks = ["${google_compute_http_health_check.default.self_link}"]

  lifecycle {
    ignore_changes = [
      "enable_cdn",
    ]
  }
}

resource "google_compute_http_health_check" "default" {
  name = "mirror-${var.env}-b"
  request_path = "/.online"
  check_interval_sec = 30
  timeout_sec = 15
}

resource "google_compute_url_map" "default" {
  name = "mirror-${var.env}-b"
  default_service = "${google_compute_backend_service.main.self_link}"

 host_rule {
    hosts        = ["*"]
    path_matcher = "allpaths"
  }

  path_matcher {
    name            = "allpaths"
    default_service = "${google_compute_backend_service.main.self_link}"

    path_rule {
      paths   = ["/debian-security", "/debian-security/", "/debian-security/*"]
      service = "${google_compute_backend_service.security.self_link}"
    }
  }
}

resource "google_compute_target_http_proxy" "default" {
  name = "mirror-${var.env}-b"
  url_map = "${google_compute_url_map.default.self_link}"
}

resource "google_compute_global_forwarding_rule" "default" {
  count = "${length(var.lb_global_ip)}"
  name = "mirror-${var.env}-b-${count.index}"
  target = "${google_compute_target_http_proxy.default.self_link}"
  ip_address = "${var.lb_global_ip[count.index]}"
  port_range = "80"
}
