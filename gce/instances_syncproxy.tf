resource "google_compute_instance" "syncproxy-main" {
  name = "mirror-${var.env}-s-m"
  zone = "${var.zone_syncproxy}"
  machine_type = "${var.instance_type_syncproxy}"

  labels = {
    environment = "${var.env}",
    purpose = "syncproxy-main",
    project = "mirror",
  }

  tags = [
    "purpose---syncproxy-main",
    "mirror-${var.env}",
    "mirror-${var.env}-syncproxy",
  ]

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
      type = "pd-ssd"
    }
  }

  attached_disk {
    source = "${google_compute_disk.syncproxy-main.*.self_link[count.index]}"
  }

  network_interface {
    subnetwork = "mirror"
    access_config {
      // Ephemeral IP
    }
  }

  lifecycle {
    ignore_changes = [ "machine_type" ]
  }
}

resource "google_compute_disk" "syncproxy-main" {
  name = "mirror-${var.env}-s-m-data"
  zone = "${var.zone_syncproxy}"
  size = "${var.disk_size_main}"
  snapshot = "${var.disk_snapshot_main}"
  type = "pd-ssd"

  lifecycle {
    ignore_changes = [ "snapshot" ]
  }
}

resource "google_compute_instance" "syncproxy-security" {
  name = "mirror-${var.env}-s-s"
  zone = "${var.zone_syncproxy}"
  machine_type = "${var.instance_type_syncproxy}"

  labels = {
    environment = "${var.env}",
    purpose = "syncproxy-security"
    project = "mirror",
  }

  tags = [
    "purpose---syncproxy-security",
    "mirror-${var.env}",
    "mirror-${var.env}-syncproxy",
  ]

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
      type = "pd-ssd"
    }
  }

  attached_disk {
    source = "${google_compute_disk.syncproxy-security.*.self_link[count.index]}"
  }

  network_interface {
    subnetwork = "mirror"
    access_config {
      // Ephemeral IP
    }
  }

  lifecycle {
    ignore_changes = [ "machine_type" ]
  }
}

resource "google_compute_disk" "syncproxy-security" {
  name = "mirror-${var.env}-s-s-data"
  zone = "${var.zone_syncproxy}"
  size = "${var.disk_size_security}"
  snapshot = "${var.disk_snapshot_security}"
  type = "pd-ssd"

  lifecycle {
    ignore_changes = [ "snapshot" ]
  }
}
